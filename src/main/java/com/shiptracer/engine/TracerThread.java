package com.shiptracer.engine;

import com.shiptracer.models.AbstractModel;
import com.shiptracer.models.ModelStorage;
import com.shiptracer.models.ShipModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aTolstikhin

 */
public class TracerThread implements Runnable {

    private ActionListener updated;
    private ActionListener stopped;
    private int speed;
    private volatile boolean running = true;

    public TracerThread(int speed, ActionListener updated, ActionListener stopped) {
        this.updated = updated;
        this.speed = speed;
        this.stopped = stopped;
    }

    @Override
    public void run() {
        while (running) {
            List<AbstractModel> models = ModelStorage.getInstance().getModels();
            for (int i = models.size() - 1; i > -1; i--) {
                models.get(i).update();
                if (models.get(i).isForKill()) {
                    ModelStorage.getInstance().removeModel(models.get(i));
                }
            }
            updated.actionPerformed(new ActionEvent(this, 0, "MODELS_UPDATED"));
            running = ((ShipModel) ModelStorage.getInstance().getShip()).getPathLeght() > 1;
            try {
                Thread.sleep(1000 / speed);
            } catch (InterruptedException ex) {
                Logger.getLogger(TracerThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        stopped.actionPerformed(new ActionEvent(this, 0, "THREAD_STOPPED"));
    }

    public void stop() {
        running = false;
    }

}
