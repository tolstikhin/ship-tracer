package com.shiptracer.engine;

public interface NodeFactory {

    public AbstractNode createNode(int x, int y);

}
