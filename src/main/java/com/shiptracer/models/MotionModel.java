package com.shiptracer.models;

import java.awt.Point;
import java.awt.image.BufferedImage;

/**
 *
 * @author Aleksandt
 
 */
public abstract class MotionModel extends AbstractModel {


    protected final int speed;
    protected final Point direction;
    protected final Point delta;

    public MotionModel(Point p, BufferedImage image, int speed, Point direction) {
        super(p, image);
        this.speed = speed;
        this.direction = direction;
        if (direction != null) {
            delta = new Point(p.x - direction.x > 0 ? -speed : speed, p.y - direction.y > 0 ? -speed : speed);
        } else {
            delta = null;
        }
    }

    @Override
    public void update() {

        p.x += delta.x;
        p.y += delta.y;
    }



}
