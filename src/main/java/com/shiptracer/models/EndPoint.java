package com.shiptracer.models;

import com.shiptracer.utils.ResourceLoader;
import java.awt.Point;

/**
 *
 * @author Александр Игоревич Толстихин (a_tol@supertel.ru)

 */
public class EndPoint extends AbstractModel {

    public EndPoint(Point p) {
        super(p, ResourceLoader.IMAGES.get("point"));
    }

    @Override
    public void update() {

    }

    @Override
    public void kill() {

    }

}
