package com.shiptracer.models;

import com.shiptracer.utils.ResourceLoader;
import java.awt.Point;

/**
 *
 * @author Александр Игоревич Толстихин (a_tol@supertel.ru)

 */
public class PointModel extends AbstractModel {

    public PointModel(Point p) {
        super(p, ResourceLoader.IMAGES.get("yellowPoint"));
    }

    @Override
    public void update() {

    }

    @Override
    public void kill() {

    }

}
