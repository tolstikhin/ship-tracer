package com.shiptracer.models;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;

/**
 *
 * @author Александр Игоревич Толстихин (a_tol@supertel.ru)

 */
public abstract class AbstractModel {

    protected boolean forKill = false;
    protected final BufferedImage image;
    protected Point p;

    protected AbstractModel(Point p, BufferedImage image) {
        this.p = p;
        this.image = image;
    }

    public Point getPoint() {
        return p;
    }

    protected int getDrawX() {
        return p.x - image.getHeight() / 2;
    }

    protected int getDrawY() {
        return p.y - image.getHeight() / 2;
    }

    public void draw(Graphics g) {
        g.drawImage(image, getDrawX(), getDrawY(), null);
    }

    public boolean isForKill() {
        return forKill;
    }

    /**
     * Обновление объекта
     */
    public abstract void update();

    /**
     * Смерть объекта
     */
    public abstract void kill();
}
