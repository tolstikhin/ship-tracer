package com.shiptracer.models;

import com.shiptracer.utils.MapUtils;
import com.shiptracer.utils.ResourceLoader;
import java.awt.Point;

/**
 *
 * @author Александр Игоревич Толстихин (a_tol@supertel.ru)

 */
public class IcebergModel extends MotionModel {

    private static final int SPEED = 1;

    public IcebergModel(Point p, Point direction) {
        super(p, ResourceLoader.IMAGES.get("iceberg"), SPEED, direction);
    }

    @Override
    public void update() {
        //проверка, что айсберг не выходит за пределы карты
        if (p.x > 0 && p.y > 0 && p.x < 400 && p.y < 400) {
            //получаем карту динамических препядствий 
            int[][] map = ModelStorage.getInstance().getIcebergMap();
            //обновление карты динамический препядствий, ставим что препядствие ушшло
            MapUtils.updateMap(map, MapUtils.getScale(p.x), MapUtils.getScale(p.y), 2, true);
            //проверка на То что объект еще живой и не ушел за дапазоны карты, и не вышел на берег
            forKill = ModelStorage.getInstance().getMap()[MapUtils.getScale(p.x)][MapUtils.getScale(p.y)] == -1;
            //обновление движения
            super.update();
            //добавляем на  карту динамических препядствий перемещенный объект
            MapUtils.updateMap(map, MapUtils.getScale(p.x), MapUtils.getScale(p.y), 2, false);
        } else {
            forKill = true;
        }
    }

    @Override
    public void kill() {

    }

}
