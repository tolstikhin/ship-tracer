/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shiptracer.models;

import com.shiptracer.utils.MapUtils;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Александр Игоревич Толстихин (a_tol@supertel.ru)
 */
public class ModelStorage {

    private int[][] map = null;
    private int[][] icebergMap = null;
    private BufferedImage image = null;
    private List<AbstractModel> models = new ArrayList<>();
    private List<AbstractModel> points = new ArrayList<>();
    private AbstractModel ship;
    private AbstractModel endPoint;

    private ModelStorage() {
    }

    public void clear() {
        map = null;
        icebergMap = null;
        image = null;
        models.clear();
        points.clear();
        ship = null;
        endPoint = null;
    }

    public static ModelStorage getInstance() {
        return ModelStorageHolder.INSTANCE;
    }

    private static class ModelStorageHolder {

        private static final ModelStorage INSTANCE = new ModelStorage();
    }

    public List<AbstractModel> getModels() {
        return Collections.unmodifiableList(models);
    }

    public AbstractModel getShip() {
        return ship;
    }

    public List<AbstractModel> getPoints() {
        return points;
    }

    public void removeEndPoint() {
        if (endPoint != null) {
            models.remove(endPoint);
        }
        endPoint = null;
    }

    public void addShip(AbstractModel ship) {
        this.ship = ship;
        this.models.add(ship);
    }

    public void addEndPoint(AbstractModel point) {
        if (this.endPoint != null) {
            this.models.remove(this.endPoint);
        }
        this.endPoint = point;
        this.models.add(endPoint);
    }

    public AbstractModel getEndPoint() {
        return endPoint;
    }

    public void addIceberg(AbstractModel iceberg) {
        this.models.add(iceberg);
    }

    public void removeModel(AbstractModel model) {
        this.models.remove(model);
    }

    public void setMap(int[][] map) {
        this.map = map;
        this.icebergMap = new int[map.length][map[0].length];
    }

    public int[][] getMap() {
        return map;
    }

    public int[][] getIcebergMap() {
        return icebergMap;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public BufferedImage getImageMap() {
        return image;
    }

    public AbstractModel findNearestIceberg(Point point) {
        AbstractModel nearestModel = null;
        double bestMatch = Double.MAX_VALUE;
        for (AbstractModel model : models) {
            double distance = MapUtils.getDistance(point, model.getPoint());
            if (distance < bestMatch) {
                bestMatch = distance;
                nearestModel = model;
            }
        }
        return nearestModel;
    }

}
