package com.shiptracer.models;

import com.shiptracer.engine.Node;
import com.shiptracer.engine.Map;
import com.shiptracer.engine.MapNodeFactory;
import com.shiptracer.utils.MapUtils;
import com.shiptracer.utils.ResourceLoader;
import java.awt.Point;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Александр Игоревич Толстихин (a_tol@supertel.ru)

 */
public class ShipModel extends MotionModel {

    private Point lastPoint;
    private double distance = 0;
    private static final int SPEED = 2;
    private AbstractModel endPoint;
    private int pathLeght;

    public ShipModel(Point p) {
        super(p, ResourceLoader.IMAGES.get("ship"), SPEED, null);
        this.lastPoint = p;
    }

    @Override
    public void update() {
        int[][] map = ModelStorage.getInstance().getMap();
        int[][] icebergMap = ModelStorage.getInstance().getIcebergMap();

        endPoint = ModelStorage.getInstance().getEndPoint();

        Map<Node> myMap = new Map<Node>(map.length, map[0].length, new MapNodeFactory(MapUtils.split(map, icebergMap)));
        List<Node> path = myMap.findPath(MapUtils.getScale(p.x), MapUtils.getScale(p.y), MapUtils.getScale(endPoint.getDrawX()), MapUtils.getScale(endPoint.getDrawY()));
        pathLeght = path.size();
        ModelStorage.getInstance().getPoints().clear();
        if (!path.isEmpty()) {
            p.x = path.get(0).getxPosition() * 10;
            p.y = path.get(0).getyPosition() * 10;
            if (p.x != lastPoint.x && p.y != lastPoint.y) {
                distance += 1.4142;
            } else {
                distance += 1;
            }
            for (Node node : path) {
                int x = node.getxPosition() * 10;
                int y = node.getyPosition() * 10;
                ModelStorage.getInstance().getPoints().add(new PointModel(new Point(x + 5, y + 5)));
            }
            lastPoint = new Point(p.x, p.y);
        } else {
            JOptionPane.showMessageDialog(null, "Невозможно найти путь!", "Ошибка!", JOptionPane.ERROR_MESSAGE);
        }
    }

    public double getDistance() {
        return Math.round(distance * 10000.0) / 10000.0;
    }

    public int getPathLeght() {
        return pathLeght;
    }

    @Override
    public void kill() {

    }
}
