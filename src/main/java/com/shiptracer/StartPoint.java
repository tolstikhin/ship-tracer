/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shiptracer;

import com.shiptracer.panels.MainFrame;

/**
 *
 * @author Александр Игоревич Толстихин (a_tol@supertel.ru)
 */
public class StartPoint {

    private static MainFrame mainFrame;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       mainFrame = new MainFrame();
       mainFrame.setVisible(true);
    }
    
}
