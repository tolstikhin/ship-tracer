package com.shiptracer.utils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author aTolstikhin

 */
public class ResourceLoader {

    public static final Map<String, BufferedImage> IMAGES = new HashMap<>();

    static {

        try {
            IMAGES.put("ship", ImageIO.read(ResourceLoader.class.getResourceAsStream("/ship.png")));
            IMAGES.put("iceberg", ImageIO.read(ResourceLoader.class.getResourceAsStream("/iceberg.png")));
            IMAGES.put("point", ImageIO.read(ResourceLoader.class.getResourceAsStream("/point.png")));
            IMAGES.put("yellowPoint", ImageIO.read(ResourceLoader.class.getResourceAsStream("/yellowPoint.png")));

        } catch (IOException ex) {
            Logger.getLogger(ResourceLoader.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
