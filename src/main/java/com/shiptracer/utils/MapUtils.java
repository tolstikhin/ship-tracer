package com.shiptracer.utils;

import com.shiptracer.models.ModelStorage;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author aTolstikhin

 */
public class MapUtils {

    private static final int BITMAP_EARTH_COLOR = -11206791;

    private static final int WATER_COLOR = 0;
    private static final int EARTH_COLOR = 100;
    private static final int SCALE_FACTOR = 10;

    /**
     * Получение из спрайт карты булевскую карту
     *
     * @param file - спрайт карта
      - булевская карта
     */
    public static int[][] getLiMap(File file) {
        //массив ли
        int[][] map = null;
        try {
            //чтение изображения
            BufferedImage image = ImageIO.read(file);
            // создаем массив
            map = new int[image.getWidth()][image.getHeight()];
            //пробегаемся по всем пикселям изображения
            for (int x = 0; x < image.getWidth(); x += 1) {
                for (int y = 0; y < image.getHeight(); y += 1) {
                    //парсим изображение
                    map[x][y] = image.getRGB(x, y) == BITMAP_EARTH_COLOR ? -1 : -2;
                }
            }
            //сохраняем картинку для отображения
            ModelStorage.getInstance().setImage(image);
        } catch (IOException ex) {
            Logger.getLogger(MapUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        //возхвращаем массив ЛИ
        return map;
    }

    /**
     * Расчет расстояния между двумя точками
     *
     * @param p1 - точка 1
     * @param p2 - точка 2
     
     */
    public static double getDistance(Point p1, Point p2) {
        return Math.sqrt(Math.pow((p2.x - p1.x), 2) + Math.pow((p2.y - p1.y), 2));
    }

    /**
     * Уменьшение размеров координат
     *
     
     
     */
    public static int getScale(int i) {
        return (int) i / SCALE_FACTOR;
    }

    /**
     * Обновить карту
     *
     * @param map - карта
     * @param x1 - х
     * @param y1 - у
     * @param r - радиус
     * @param runnable - возможно ли идли
      - карта
     */
    public static int[][] updateMap(int[][] map, int x1, int y1, int r, boolean runnable) {
        for (int x = x1 - r; x < x1 + r; x += 1) {
            for (int y = y1 - r; y < y1 + r; y += 1) {
                if (x > 0 && y > 0 && x < map.length && y < map[0].length) {
                    map[x][y] = runnable ? -2 : -1;
                }
            }
        }
        return map;
    }

    public static int[][] split(int[][] map1, int map2[][]) {
        int[][] newmap = new int[map1.length][map1[0].length];

        for (int x = 0; x < map1.length; x += 1) {
            for (int y = 0; y < map1.length; y += 1) {
                newmap[x][y] = map1[x][y];
            }
        }
        for (int x = 0; x < map1.length; x += 1) {
            for (int y = 0; y < map1.length; y += 1) {
                if (map2[x][y] == -1) {
                    newmap[x][y] = map2[x][y];
                }
            }
        }
        return newmap;
    }
}
