/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shiptracer.panels;

import com.shiptracer.engine.TracerThread;
import com.shiptracer.models.ModelStorage;
import com.shiptracer.panels.mapViewer.Action;
import com.shiptracer.panels.mapViewer.MapCanvas;
import com.shiptracer.utils.MapUtils;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Александр Игоревич Толстихин (a_tol@supertel.ru)
 */
public class MainFrame extends javax.swing.JFrame {

    private TracerThread updateThread;
    private MapCanvas canvas = new MapCanvas();

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initlLookAndFeel();
        canvas.setVisible(false);
        initComponents();
        jPanel1.setVisible(false);
        jPanelCanvasWrapper.add(canvas);
        canvas.setActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof Action) {
                    Action action = (Action) e.getSource();
                    jLabelStatus.setText(action.getName());
                }
                if (e.getSource() instanceof Double) {
                    jLabelStatus.setText("Пройденный путь: " + (Double) e.getSource() + " миль");
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelCanvasWrapper = new javax.swing.JPanel();
        jPanelButtonsWrapper = new javax.swing.JPanel();
        jPanelOptionsPanel = new javax.swing.JPanel();
        jPanelMap = new javax.swing.JPanel();
        jButtonLoadMap = new javax.swing.JButton();
        jPanelShip = new javax.swing.JPanel();
        jButtonSetShip = new javax.swing.JButton();
        jButtonEndPoint = new javax.swing.JButton();
        jPanelIceberg = new javax.swing.JPanel();
        jButtonAddIceberg = new javax.swing.JButton();
        jButtonRemoveIceberg = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabelRadiusIceberg = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jSliderRadiusIceberg = new javax.swing.JSlider();
        jPanelEmulationOption = new javax.swing.JPanel();
        jButtonStart = new javax.swing.JButton();
        jButtonStop = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jSliderSpeed = new javax.swing.JSlider();
        jLabel4 = new javax.swing.JLabel();
        jLabelSpeed = new javax.swing.JLabel();
        jPanelStatus = new javax.swing.JPanel();
        jLabelStatus = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ShipTracer Влада Исаченко, группа 2493");
        setResizable(false);

        jPanelCanvasWrapper.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanelCanvasWrapper.setLayout(new java.awt.CardLayout());
        getContentPane().add(jPanelCanvasWrapper, java.awt.BorderLayout.CENTER);

        jPanelButtonsWrapper.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jPanelOptionsPanel.setLayout(new javax.swing.BoxLayout(jPanelOptionsPanel, javax.swing.BoxLayout.PAGE_AXIS));

        jPanelMap.setBorder(javax.swing.BorderFactory.createTitledBorder("Карта"));
        jPanelMap.setLayout(new java.awt.GridBagLayout());

        jButtonLoadMap.setText("Загрузить карту");
        jButtonLoadMap.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLoadMapActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanelMap.add(jButtonLoadMap, gridBagConstraints);

        jPanelOptionsPanel.add(jPanelMap);

        jPanelShip.setBorder(javax.swing.BorderFactory.createTitledBorder("Корабль"));
        jPanelShip.setLayout(new java.awt.GridBagLayout());

        jButtonSetShip.setText("Установить начальную точку");
        jButtonSetShip.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSetShipActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanelShip.add(jButtonSetShip, gridBagConstraints);

        jButtonEndPoint.setText("Установить конечную точку");
        jButtonEndPoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEndPointActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanelShip.add(jButtonEndPoint, gridBagConstraints);

        jPanelOptionsPanel.add(jPanelShip);

        jPanelIceberg.setBorder(javax.swing.BorderFactory.createTitledBorder("Айсберги"));
        jPanelIceberg.setLayout(new java.awt.GridBagLayout());

        jButtonAddIceberg.setText("+");
        jButtonAddIceberg.setActionCommand("Добавить");
        jButtonAddIceberg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddIcebergActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanelIceberg.add(jButtonAddIceberg, gridBagConstraints);

        jButtonRemoveIceberg.setText("-");
        jButtonRemoveIceberg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoveIcebergActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanelIceberg.add(jButtonRemoveIceberg, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("м");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        jPanel1.add(jLabel3, gridBagConstraints);

        jLabelRadiusIceberg.setText("00");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel1.add(jLabelRadiusIceberg, gridBagConstraints);

        jLabel1.setText("Радиус обхода айсберга:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel1.add(jLabel1, gridBagConstraints);

        jSliderRadiusIceberg.setMaximum(4);
        jSliderRadiusIceberg.setMinimum(1);
        jSliderRadiusIceberg.setSnapToTicks(true);
        jSliderRadiusIceberg.setValue(1);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel1.add(jSliderRadiusIceberg, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanelIceberg.add(jPanel1, gridBagConstraints);

        jPanelOptionsPanel.add(jPanelIceberg);

        jPanelEmulationOption.setBorder(javax.swing.BorderFactory.createTitledBorder("Эмуляция"));
        jPanelEmulationOption.setLayout(new java.awt.GridBagLayout());

        jButtonStart.setText("Старт");
        jButtonStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonStartActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.weightx = 0.1;
        jPanelEmulationOption.add(jButtonStart, gridBagConstraints);

        jButtonStop.setText("Стоп");
        jButtonStop.setEnabled(false);
        jButtonStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonStopActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.weightx = 0.1;
        jPanelEmulationOption.add(jButtonStop, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jSliderSpeed.setMaximum(10);
        jSliderSpeed.setMinimum(3);
        jSliderSpeed.setValue(6);
        jSliderSpeed.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                jSliderSpeedCaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel2.add(jSliderSpeed, gridBagConstraints);

        jLabel4.setText("Скорость отображения:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jLabel4, gridBagConstraints);

        jLabelSpeed.setText("00");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel2.add(jLabelSpeed, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanelEmulationOption.add(jPanel2, gridBagConstraints);

        jPanelOptionsPanel.add(jPanelEmulationOption);

        jPanelButtonsWrapper.add(jPanelOptionsPanel);

        getContentPane().add(jPanelButtonsWrapper, java.awt.BorderLayout.EAST);

        jPanelStatus.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 10, 5));
        jPanelStatus.add(jLabelStatus);

        getContentPane().add(jPanelStatus, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSetShipActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSetShipActionPerformed
        canvas.setAction(Action.SET_SHIP);
    }//GEN-LAST:event_jButtonSetShipActionPerformed

    private void jButtonAddIcebergActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddIcebergActionPerformed
        canvas.setAction(Action.SET_NEW_ICEBERG);
    }//GEN-LAST:event_jButtonAddIcebergActionPerformed

    private void jButtonLoadMapActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLoadMapActionPerformed
        ModelStorage.getInstance().clear();
        Preferences pref = Preferences.userRoot();
        JFileChooser chooser = new JFileChooser(pref.get("LOAD_PATH", ""));
        chooser.setMultiSelectionEnabled(false);

        int returnVal = chooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            pref.put("LOAD_PATH", chooser.getSelectedFile().getPath());
            ModelStorage.getInstance().setMap(MapUtils.getLiMap(chooser.getSelectedFile()));
        }
        canvas.setPreferredSize(new Dimension(400, 400));
        canvas.repaint();
        this.pack();
    }//GEN-LAST:event_jButtonLoadMapActionPerformed

    private void jButtonRemoveIcebergActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoveIcebergActionPerformed
        canvas.setAction(Action.DELETE_ICEBERG);
    }//GEN-LAST:event_jButtonRemoveIcebergActionPerformed

    private void jButtonStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonStartActionPerformed
        updateThread = new TracerThread(jSliderSpeed.getValue(), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.repaint();
            }
        }, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jButtonStart.setEnabled(true);
                jButtonStop.setEnabled(false);
                ModelStorage.getInstance().removeEndPoint();
                canvas.repaint();
            }
        });
        new Thread(updateThread).start();
        jButtonStart.setEnabled(false);
        jButtonStop.setEnabled(true);
    }//GEN-LAST:event_jButtonStartActionPerformed

    private void jButtonEndPointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEndPointActionPerformed
        canvas.setAction(Action.SET_END_POINT);
    }//GEN-LAST:event_jButtonEndPointActionPerformed

    private void jButtonStopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonStopActionPerformed
        updateThread.stop();
        jButtonStart.setEnabled(true);
        jButtonStop.setEnabled(false);
    }//GEN-LAST:event_jButtonStopActionPerformed

    private void jSliderSpeedCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_jSliderSpeedCaretPositionChanged
        jLabelSpeed.setText(String.valueOf(jSliderSpeed.getValue()));
    }//GEN-LAST:event_jSliderSpeedCaretPositionChanged

    public void initlLookAndFeel() {
        try {
            UIManager.setLookAndFeel(new WindowsLookAndFeel());
        } catch (UnsupportedLookAndFeelException e) {
            System.err.println("Can't use the specified look and feel on this platform.");
        } catch (Exception e) {
            System.err.println("Couldn't get specified look and feel, for some reason.");
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddIceberg;
    private javax.swing.JButton jButtonEndPoint;
    private javax.swing.JButton jButtonLoadMap;
    private javax.swing.JButton jButtonRemoveIceberg;
    private javax.swing.JButton jButtonSetShip;
    private javax.swing.JButton jButtonStart;
    private javax.swing.JButton jButtonStop;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelRadiusIceberg;
    private javax.swing.JLabel jLabelSpeed;
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanelButtonsWrapper;
    private javax.swing.JPanel jPanelCanvasWrapper;
    private javax.swing.JPanel jPanelEmulationOption;
    private javax.swing.JPanel jPanelIceberg;
    private javax.swing.JPanel jPanelMap;
    private javax.swing.JPanel jPanelOptionsPanel;
    private javax.swing.JPanel jPanelShip;
    private javax.swing.JPanel jPanelStatus;
    private javax.swing.JSlider jSliderRadiusIceberg;
    private javax.swing.JSlider jSliderSpeed;
    // End of variables declaration//GEN-END:variables
}
