/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shiptracer.panels.mapViewer;

/**
 *
 * @author Александр Игоревич Толстихин (a_tol@supertel.ru)

 */
public enum Action {
    NONE(""),
    SET_SHIP("Установите точку корабля, нажав на карту"),
    SET_END_POINT("Установите точку прибытия, нажав на карту "),
    SET_NEW_ICEBERG("Установите айсберг, нажав на карту"),
    SET_ICEBERG_DIRECTION("Установите направление движения айсберга, нажав на карту"),
    DELETE_ICEBERG("Удалите айсберг нажав на него");

    private String name;

    private Action(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
