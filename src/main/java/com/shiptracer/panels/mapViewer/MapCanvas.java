package com.shiptracer.panels.mapViewer;

import com.shiptracer.models.AbstractModel;
import com.shiptracer.models.EndPoint;
import com.shiptracer.models.IcebergModel;
import com.shiptracer.models.ModelStorage;
import com.shiptracer.models.ShipModel;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author Александр Игоревич Толстихин (a_tol@supertel.ru)

 */
public class MapCanvas extends JPanel {

    private Action action = Action.NONE;
    private ActionListener actionListener;

    public MapCanvas() {
        addMouseListener(new MapListner());
    }

    @Override
    protected void paintComponent(Graphics g) {
        paintMap(g);
        for (AbstractModel model : ModelStorage.getInstance().getModels()) {
            model.draw(g);
            if (model instanceof ShipModel) {
                actionListener.actionPerformed(new ActionEvent(((ShipModel) model).getDistance(), 0, "ACTION_CHANGE"));
            }
        }
        for (AbstractModel model : ModelStorage.getInstance().getPoints()) {
            model.draw(g);
        }
    }

    private void paintMap(Graphics g) {
        BufferedImage image = ModelStorage.getInstance().getImageMap();
        if (image != null) {
            g.drawImage(image.getScaledInstance(400, 400, Image.SCALE_SMOOTH), 0, 0, null);
        }
    }

    public void setAction(Action action) {
        this.action = action;
        actionListener.actionPerformed(new ActionEvent(action, 0, "ACTION_CHANGE"));
    }

    private class MapListner extends MouseAdapter {

        private Point icebergPoint;

        @Override
        public void mouseClicked(MouseEvent e) {
            switch (action) {
                case SET_SHIP:
                    ModelStorage.getInstance().addShip(new ShipModel(e.getPoint()));
                    setAction(Action.NONE);
                    break;
                case SET_END_POINT:
                    ModelStorage.getInstance().addEndPoint(new EndPoint(e.getPoint()));
                    setAction(Action.NONE);
                    break;
                case SET_NEW_ICEBERG:
                    icebergPoint = e.getPoint();
                    setAction(Action.SET_ICEBERG_DIRECTION);
                    break;
                case SET_ICEBERG_DIRECTION:
                    ModelStorage.getInstance().addIceberg(new IcebergModel(icebergPoint, e.getPoint()));
                    setAction(Action.NONE);
                    break;
                case DELETE_ICEBERG:
                    AbstractModel model = ModelStorage.getInstance().findNearestIceberg(e.getPoint());
                    ModelStorage.getInstance().removeModel(model);
            }
        }

    }

    public void setActionListener(ActionListener actionListener) {
        this.actionListener = actionListener;
    }

}
