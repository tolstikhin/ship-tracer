##  SHIP TRACER  ##
### Трассировка пути корабля ###
-----
Трассировка пути корабля по алгоритму А*, в обход внешних факторов, как статических так и динамических.

-----
Trace the path of the ship by the algorithm A *, bypassing the external static and dynamic factors.